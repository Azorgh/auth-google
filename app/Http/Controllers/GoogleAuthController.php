<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use OAuth;

class GoogleAuthController extends Controller {

	public function index(Request $request){
		$code = $request->get('code');
		$googleService = \OAuth::consumer('Google');

		if ( ! is_null($code))
		{
			$token = $googleService->requestAccessToken($code);
			$result = json_decode($googleService->request('https://www.googleapis.com/oauth2/v1/userinfo'), true);
			return view("google.index", compact('result'));
		}else{
			$googleService = OAuth::consumer('Google');
			$url = $googleService->getAuthorizationUri();
			return redirect((string)$url);
		}
	}

}
