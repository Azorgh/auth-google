@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-offset-1 col-sm-5">
                <div class="panel panel-default panel-google-plus">
                    <div class="panel-heading">
                        <img class="img-circle pull-left" src="{{ $result['picture'] }}" alt="Mouse0270" />
                        <h3>{{ $result['name'] }}</h3>
                    </div>
                    <div class="panel-body">

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop